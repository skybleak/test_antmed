<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CreateStreamingController;
use App\Http\Controllers\StreamingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])
    ->name('home');

Route::get('/dashboard', function() {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/create-stream', [CreateStreamingController::class, 'index'])
    ->name('create-stream-index');

Route::get('/stream/{id}', [StreamingController::class, 'show'])->name('stream-show');

Route::post('/create-stream', [CreateStreamingController::class, 'create'])
    ->name('create-stream-create');

require __DIR__ . '/auth.php';
