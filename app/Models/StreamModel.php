<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $name
 * @property mixed $description
 * @property mixed $preview
 * @property mixed $author
 * @property mixed $streamExtId
 */
class StreamModel extends Model {
    use HasFactory;

    protected $table = 'stream';
}
