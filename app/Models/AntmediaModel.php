<?php

namespace App\Models;

class AntmediaModel {

    const URL = 'http://89.22.229.228:5080/';

    public function create($name) {
        $data = [
            'name' => $name,
        ];

        $post_data = json_encode($data);

        $crl = curl_init(self::URL . 'LiveApp/rest/v2/broadcasts/create');
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($crl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

        $result = curl_exec($crl);

        if ($result) {
            $data = json_decode($result, true);

            return $data['streamId'];
        }

        return false;
    }
}
