<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PreviewModel {

    public function upload(Request $request) {
        return $request->file('preview')->store('files');
    }
}
