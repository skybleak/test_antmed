<?php

namespace App\Repositories;

use App\Models\AntmediaModel;
use App\Models\PreviewModel;
use App\Models\StreamModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StreamRepository {

    /**
     * @param $id
     * @return object|null
     */
    public function findById($id): object|null {
        return StreamModel::query()->where('id', $id)->first();
    }

    /**
     * @return Collection
     */
    public function findAll(): Collection {
        return StreamModel::all();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function save(Request $request): bool {
        $streamModel = new StreamModel();

        $streamModel->name = $request->get('name');
        $streamModel->description = $request->get('description');
        $streamModel->preview = (new PreviewModel())->upload($request);
        $streamModel->author = Auth::user()->name;

        $streamModel->save();

        if ($streamModel->save()) {
            $streamExtId = (new AntmediaModel())->create($streamModel->name);

            $streamModel->streamExtId = $streamExtId;
            $streamModel->save();
        }

        return true;
    }
}
