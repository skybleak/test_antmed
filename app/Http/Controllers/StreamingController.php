<?php

namespace App\Http\Controllers;

use App\Repositories\StreamRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class StreamingController {
    /**
     * @param $id
     * @return Factory|View|Application
     */
    public function show($id): Factory|View|Application {
        $stream = (new StreamRepository())->findById($id);

        return view('stream-show', [
            'stream' => $stream
        ]);
    }
}
