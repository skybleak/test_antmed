<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStreamRequest;
use App\Repositories\StreamRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CreateStreamingController {

    public function index() {
        return view('create-stream');
    }

    /**
     * @param CreateStreamRequest $request
     */
    public function create(CreateStreamRequest $request) {
        (new StreamRepository())->save($request);

        return redirect()->route('create-stream-index');
    }
}
