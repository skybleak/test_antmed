<?php

namespace App\Http\Controllers;

use App\Repositories\StreamRepository;
use Illuminate\Support\Facades\Storage;

class HomeController {
    public function index() {
        $streams = (new StreamRepository())->findAll();

        return view('index', [
            'streams' => $streams
        ]);
    }
}
